<?php

	class m_attach extends MY_Model
	{
		protected $_table_name = 'attachment';
		protected $_order_by = 'name';
		
		public function __construct()
		{
			parent::__construct();
		}

		public function gettAttachment($id)
		{
			
			$query = $this->db->get_where('attachment', array('game_id' => $id));
			return $query->result();
		
		}
	}