<?php

	class m_login extends MY_Model
	{
		protected $_table_name = 'user';
		protected $_order_by = 'username';
		
		public function __construct()
		{
			parent::__construct();
		}
		
		public function login($user, $pass)
		{
			$user = $this->get_by( 

				array (
					'username' => $user,
					'password' => $pass,
				));
			

			if($user)
			{
				foreach ($user as $row)
				{
					$data = array (
						'username'   		=> $row->username,
						'password' 		=> $row->password ,
						'loggedin' 		=> TRUE
					);
					$this->session->set_userdata($data);
					return true;
				}
			}
			else{
				return false;
				
			}
		}
		public function logout()
		{
			$this->session->sess_destroy();
		}
	}