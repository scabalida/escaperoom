<?php defined('BASEPATH') OR exit('No direct script access allowed');
	
	class policy extends Admin_Controller {
		public function __construct() {
      parent::__construct();
	  $this->load->model('m_admin');
    }
    
		public function index() {
			$data['room'] = $this->m_admin->getRoom();
			$this->load->view('policy/header');
			$this->load->view('policy/policy',$data);
			$this->load->view('policy/footer');
		}
		

		// End Dashboard Class
	}