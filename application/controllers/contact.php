<?php defined('BASEPATH') OR exit('No direct script access allowed');
	
	class contact extends CI_Controller  {
		public function __construct() {
		
      parent::__construct();
	  $this->load->helper('form');
    }
    
		public function index() {
			
			$this->load->view('contact/header');
			$this->load->view('contact/contact');
			$this->load->view('contact/footer');
		}

		 public function sendEmail()
 	{
 	
	    $this->load->library('email');
		$this->email->from($this->input->post('inputEmail'), $this->input->post('inputName'));
		$this->email->to('office@escaperoomfactory.net');

		$this->email->subject($this->input->post('inputSubject'));
		$this->email->message($this->input->post('inputMessage'));

		if($this->email->send()){
			echo json_encode('true');
						
					}
					else{
						echo json_encode('false');
						
					}
		
 	}
		// End Dashboard Class
	}