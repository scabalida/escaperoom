<?php defined('BASEPATH') OR exit('No direct script access allowed');
	
	class games extends Admin_Controller {
		public function __construct() {
      parent::__construct();
	  $this->load->model('m_admin');
	    $this->load->helper('form');
    }
    
		public function index() {
			
			$data['room'] = $this->m_admin->getRoom();
			$this->load->view('games/header');
			$this->load->view('games/game',$data);
			$this->load->view('games/footer');
		}

		public function info($id) {
			$data1['info'] = $this->m_admin->get($id, $single = FALSE);
			$this->load->view('games/header');
			$this->load->view('games/info', $data1);
			$this->load->view('games/footer');
		}
		public function payNow(){
			 $this->load->library('email');
			$attach = $this->m_admin->getAttachment($this->input->post('id'));
			$this->email->from('office@escaperoomfactory.net', 'Escaperoom Factory');
			$this->email->to($this->input->post('email'));

			$this->email->subject('Escaperoom Files');
			$this->email->message('Thanks.');
				
			$path = 'upload/'.$this->input->post('id');
			foreach ($attach as $row)
			{
				$this->email->attach($path."/".$row->name);
			}
			
			if($this->email->send()){
				echo json_encode('true');
						
			}
			else{
				echo json_encode('false');
					
			}
	
		
		
		//print_r($attach);
		///echo	$this->input->post('name');
		//echo	$this->input->post('id');
		//echo	$this->input->post('contact');
		//echo	$this->input->post('email');
		//echo	$this->input->post('address');
		}
		// End Dashboard Class
	}