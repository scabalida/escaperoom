<?php defined('BASEPATH') OR exit('No direct script access allowed');
	
	class about extends Admin_Controller {
		public function __construct() {
      parent::__construct();
    }
    
		public function index() {
			
			$this->load->view('about/header');
			$this->load->view('about/about');
			$this->load->view('about/footer');
		}
		

		// End Dashboard Class
	}