<?php defined('BASEPATH') OR exit('No direct script access allowed');
	
	class faq extends Admin_Controller {
		public function __construct() {
      parent::__construct();
    }
    
		public function index() {
			
			$this->load->view('faq/header');
			$this->load->view('faq/faq');
			$this->load->view('faq/footer');
		}
		

		// End Dashboard Class
	}