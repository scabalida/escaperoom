<?php defined('BASEPATH') OR exit('No direct script access allowed');
	
	class login extends Admin_Controller {
		public function __construct() {
      parent::__construct();
	  $this->load->model('m_login');
    }
    
		public function index() {
			
		
			$this->load->view('home/header');
			$this->load->view('home/login');
			$this->load->view('home/footer');
		}
		
		public function loginNow(){
				

				$username =  $this->input->post('inputUsername');
				$password =  md5($this->input->post('inputPassword'));
			
			if($this->m_login->login($username, $password)){
						
				echo json_encode('true');
							
					}
			else{
				
			echo json_encode('false');
			}
			
		}
		public function logout()
		{
			$this->m_login->logout();
			redirect('home');  
		}
		
		
		// End Dashboard Class
	}