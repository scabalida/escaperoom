<?php defined('BASEPATH') OR exit('No direct script access allowed');
	
	class home extends Admin_Controller {
		public function __construct() {
      parent::__construct();
	   $this->load->model('m_admin');
    }
    
		public function index() {
			$data['room'] = $this->m_admin->getRoom();
			$this->load->view('home/header');
			$this->load->view('home/home',$data);
			$this->load->view('home/footer');
		}


		// End Dashboard Class
	}