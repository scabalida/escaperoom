<?php defined('BASEPATH') OR exit('No direct script access allowed');
	
	class admin extends Admin_Controller {
		public function __construct() {
      parent::__construct();
	  $this->load->model('m_admin');
	  $this->load->model('m_attach');
	    $this->load->helper(array('form', 'url'));
    }
    
		public function index() {
			
			$data['room'] = $this->m_admin->getRoom();
			$this->load->view('admin/index',$data + array('error' => ' ' ));
		}
		
		public function deleteRoom($id){
			
			$data = $this->m_admin->delete($id);
			
				echo json_encode($data);
		
		}
		public function deleteAttachment($id,$game_id){
			
		
			$datas = $this->m_attach->get($id, $single = FALSE);
			unlink("upload/".$game_id."/".$datas->name);
			$data = $this->m_attach->delete($id);
			
				echo json_encode($data);
		
		}
		public function addRoom(){
			
			$data = array(
			'name' => $this->input->post('name'),
			'price' => $this->input->post('price'),
			'story' => $this->input->post('story'),
			'main_picture' => "",
			'bg_picture' => "",
			'difficulty' => $this->input->post('difficulty'),
			'policy' => $this->input->post('policy')
			
			);
			
				if($this->m_admin->save($data ) ){
						echo json_encode('true');
						
					}
					else{
						echo json_encode('false');
						
					}
		}
		public function getPolicy($id){
			
		$data = $this->m_admin->get($id, $single = FALSE);
				
						echo json_encode($data);
						
				
		}
		public function updateRoom(){
			$id = $this->input->post('id');
			$data = array(
			'name' => $this->input->post('name'),
			'price' => $this->input->post('price'),
			'story' => $this->input->post('story'),
			'difficulty' => $this->input->post('difficulty'),
			'policy' => $this->input->post('policy')
			
			);
			
				if($this->m_admin->save($data,$id ) ){
						echo json_encode('true');
						
					}
					else{
						echo json_encode('false');
						
					}
		}
		public function uploadMainPicture($id)
		{
			   $config['upload_path']          = 'upload';
                $config['allowed_types']        = 'gif|jpg|png|jpeg';

                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('main_picture'))
                {
                        $error = array('error' => $this->upload->display_errors());

                       redirect('admin');
                }
                else
                {
                        $image = $this->upload->data();
						$data = array(
							'main_picture' =>  $image['file_name']
							);
						if($this->m_admin->save($data,$id ) ){
							redirect('admin');
							
						}
						else{
							redirect('admin');
							
						}
                }
			
		}
		
		public function updateMainPicture($id)
		{
			   $config['upload_path']          = 'upload';
                $config['allowed_types']        = 'gif|jpg|png|jpeg';

                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('main_picture'))
                {
                        $error = array('error' => $this->upload->display_errors());

                    redirect('admin');
                }
                else
                {
						$oldImg = $this->input->post('old_main_picture');
						unlink("upload/".$oldImg);
						$image = $this->upload->data();
						$data = array(
							'main_picture' =>  $image['file_name']
							);
						
						if($this->m_admin->save($data,$id ) ){
							redirect('admin');
							
						}
						else{
							redirect('admin');
							
						}
                }
			
		}
		
		public function uploadBgPicture($id)
		{
			   $config['upload_path']          = 'upload';
                $config['allowed_types']        = 'gif|jpg|png|jpeg';

                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('bg_picture'))
                {
                        $error = array('error' => $this->upload->display_errors());
						redirect('admin');               
				}
                else
                {
                        $image = $this->upload->data();
						$data = array(
								'bg_picture' =>  $image['file_name']
							);
						if($this->m_admin->save($data,$id ) ){
							redirect('admin');
						}
						else{
							redirect('admin');
						}
                }
			
		}
		
		public function updateBgPicture($id)
		{
			   $config['upload_path']          = 'upload';
                $config['allowed_types']        = 'gif|jpg|png|jpeg';

                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('bg_picture'))
                {
                        $error = array('error' => $this->upload->display_errors());

                    redirect('admin');
                }
                else
                {
						$oldImg = $this->input->post('old_bg_picture');
						unlink("upload/".$oldImg);
						$image = $this->upload->data();
						$data = array(
							'bg_picture' =>  $image['file_name']
							);
						
						if($this->m_admin->save($data,$id ) ){
							redirect('admin');
							
						}
						else{
							redirect('admin');
							
						}
                }
			
		}
		function viewAttach($id)
		{ 
			$data['attach'] = $this->m_attach->gettAttachment($id);
			$this->load->view('admin/attachment', $data + array('error' => ' ', 'id' => $id ));
		}
		 function attachFile()
		{       
			$this->load->library('upload');

			$files = $_FILES;
			$cpt = count($_FILES['userfile']['name']);
			for($i=0; $i<$cpt; $i++)
			{           
				$_FILES['userfile']['name']= $files['userfile']['name'][$i];
				$_FILES['userfile']['type']= $files['userfile']['type'][$i];
				$_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
				$_FILES['userfile']['error']= $files['userfile']['error'][$i];
				$_FILES['userfile']['size']= $files['userfile']['size'][$i];    
				
				$x = "upload/".$this->input->post('id');
				 if(!is_dir($x)) //create the folder if it's not already exists
				{
					mkdir($x,0755,TRUE);
				}
				
				$this->upload->initialize($this->set_upload_options($this->input->post('id')));
				$this->upload->do_upload();
				
				$data = array(
				'name' => $files['userfile']['name'][$i] ,
				'game_id' => $this->input->post('id')
				
				);
				$this->m_attach->save($data );
			}
			
			header( 'Location: '. base_url('admin/viewAttach/'.$this->input->post('id')).'' ) ;
		}

		private function set_upload_options($id)
		{   
			//upload an image options
			$config = array();
			$config['upload_path'] = 'upload/'.$id;
			$config['allowed_types'] = 'docx|doc|png|jpg|jpeg|gif|rar|zip';

			return $config;
		}
		// End Dashboard Class
	}