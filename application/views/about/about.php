<!-- Main Wrapper Header -->
        <div class="main-wrapper-header fancy-header dark-header" data-stellar-background-ratio="0.4">
			
            <div class="bg-overlay bg-overlay-gdark"></div>

 			<div class="container">
            
				<div class="row">
                	<div class="col-sm-12 columns">
                		<div class="page-title">
                    		<h2>About Us</h2>                    
                    	</div>
                    	<div class="breadcrumbs-wrapper">               
							<ol class="breadcrumb">
  								<li><a href="#">Home</a></li>
								<li class="active">About Us</li>
							</ol>
                		</div>
					</div>
				</div>
                
			</div>
 
        </div>       
		<!-- /Main Wrapper Header -->
		<div class="main-wrapper">

        	<!-- Container -->
            <div class="container">
            	<div class="white-space space-big"></div>
                
            	<div class="row">
                    <div class="col-md-8 col-md-offset-2 columns">
                    	<h3 class="fancy-title text-center animation fadeInUp"><span>WHAT WE DO</span></h3>
                        <p class="lead text-center">Mauris vel dapibus mi. Suspendisse feugiat hendrerit ipsum non placerat.</p>
                    	<div class="white-space space-small"></div>
                    </div>
				</div>
            	<div class="row">
                    <div class="col-sm-4">

								<div class="panel panel-default animation fadeInLeft delay1">
                                	<div class="panel-body">
                                    	<!-- Team Member -->
                                    	<div class="team-wrapper team-wrapper-alt">
											<div class="team-member">
												<img class="img-responsive img-rounded" alt="Team Member" src="<?php echo base_url();?>img/demo/content/about3.jpg">
                                                <div class="team-info">
                                                	<h4 class="team-name">Branko Maricic</h4>
													<p class="team-role">Game Designer</p>
                                                </div>
                                                <hr class="hr-fancy text-center"/>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin consequat sollicitudin cursus.</p>
                                                
                                            </div>
                                        </div>
                                        <!-- /Team Member --> 										
                                    </div>
                                </div>                                 

                    </div>
                    <div class="col-sm-4">

								<div class="panel panel-default animation fadeInLeft">
                                	<div class="panel-body">
                                    	<!-- Team Member -->
                                    	<div class="team-wrapper team-wrapper-alt">
											<div class="team-member">
												<img class="img-responsive img-rounded" alt="Team Member" src="<?php echo base_url();?>img/demo/content/about3.jpg">
                                                <div class="team-info">
                                                	<h4 class="team-name">Milica Maricic</h4>
													<p class="team-role">Game Designer</p>
                                                </div>
                                                <hr class="hr-fancy text-center"/>
                                                <p>Quisque non aliquet ligula, nec lobortis sem. Morbi sit amet facilisis ipsum. Cras nisi accumsan.</p>
                                                
                                            </div>
                                        </div>
                                        <!-- /Team Member --> 										
                                    </div>
                                </div>

                   	</div>
                    <div class="col-sm-4">

								<div class="panel panel-default animation fadeInRight delay1">
                                	<div class="panel-body">
                                    	<!-- Team Member -->
                                    	<div class="team-wrapper team-wrapper-alt">
											<div class="team-member">
												<img class="img-responsive img-rounded" alt="Team Member" src="<?php echo base_url();?>img/demo/content/about3.jpg">
                                                <div class="team-info">
                                                	<h4 class="team-name">Svetlana Maricic</h4>
													<p class="team-role">Marketing Manager</p>
                                                </div>
                                                <hr class="hr-fancy text-center"/>
                                               <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin consequat sollicitudin cursus.</p>
                                                
                                            </div>
                                        </div>
                                        <!-- /Team Member --> 										
                                    </div>
                                </div>

                    </div>
				</div>
				            	<div class="row">
                    <div class="col-sm-4">

								<div class="panel panel-default  ">
                                	<div class="panel-body">
                                    	<!-- Team Member -->
                                    	<div class="team-wrapper team-wrapper-alt">
											<div class="team-member">
												<img class="img-responsive img-rounded" alt="Team Member" src="<?php echo base_url();?>img/demo/content/about3.jpg">
                                                <div class="team-info">
                                                	<h4 class="team-name">Marko Vranesevic</h4>
													<p class="team-role">Marketing Manager</p>
                                                </div>
                                                <hr class="hr-fancy text-center"/>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin consequat sollicitudin cursus.</p>
                                                
                                            </div>
                                        </div>
                                        <!-- /Team Member --> 										
                                    </div>
                                </div>                                 

						<div class="white-space space-big"></div>
                    </div>
                    <div class="col-sm-4">

								<div class="panel panel-default">
                                	<div class="panel-body">
                                    	<!-- Team Member -->
                                    	<div class="team-wrapper team-wrapper-alt">
											<div class="team-member">
												<img class="img-responsive img-rounded" alt="Team Member" src="<?php echo base_url();?>img/demo/content/about3.jpg">
                                                <div class="team-info">
                                                	<h4 class="team-name">Vladimir Petronijevic</h4>
													<p class="team-role">Financial Manager</p>
                                                </div>
                                                <hr class="hr-fancy text-center"/>
                                                <p>Quisque non aliquet ligula, nec lobortis sem. Morbi sit amet facilisis ipsum. Cras nisi accumsan.</p>
                                                
                                            </div>
                                        </div>
                                        <!-- /Team Member --> 										
                                    </div>
                                </div>

						<div class="white-space space-big"></div>
                   	</div>
                    <div class="col-sm-4">

						<div class="white-space space-big"></div>
                    </div>
				</div>
				
				<hr>
            </div>
			
		</div>