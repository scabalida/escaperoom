<!-- Main Wrapper Header -->
        <div class="main-wrapper-header fancy-header dark-header" data-stellar-background-ratio="0.4">
			
            <div class="bg-overlay bg-overlay-gdark"></div>

 			<div class="container">
            
				<div class="row">
                	<div class="col-sm-12 columns">
                		<div class="page-title">
                    		<h2>Contact Us</h2>                    
                    	</div>
                    	<div class="breadcrumbs-wrapper">               
							<ol class="breadcrumb">
  								<li><a href="#">Home</a></li>
								<li class="active">Contact Us</li>
							</ol>
                		</div>
					</div>
				</div>
                
			</div>
 
        </div>       
		<!-- /Main Wrapper Header -->

		<!-- Main Container -->
		<div class="main-wrapper">

        	<!-- Container -->
            <div class="container">
            	<div class="white-space space-big"></div>
                
            	<div class="row">
                    <div class="col-md-6">
                    	<h3 class="fancy-title"><span>Contact Information</span></h3>
                        <p>Aliquam quis fermentum justo, sit amet tincidunt turpis. Fusce varius neque eros, in accumsan felis pellentesque at. Nulla ut bibendum dui. Nunc lobortis felis a quam sagittis, nec dignissim elit laoreet. Nam tincidunt justo at turpis sollicitudin commodo. </p>
						<div class="row">	
                        	<div class="col-sm-6">
                            	<ul class="list-default fa-ul">
                        			<li><span class="fa-li fa fa-phone color-default"></span>+381621638545</li>
                        			<li><span class="fa-li fa fa fa-whatsapp color-default"></span>+381621638545</li>                           
                        			<li><span class="fa-li fa fa fa-skype color-default"></span>Milica Maricic</li>                           
                        		</ul>
                            </div>
                        	<div class="col-sm-6">
                            	<ul class="list-default fa-ul">
                        			<li><span class="fa-li fa fa-map-marker color-default"></span>Novi Sad, Serbia </li>
                            		<li><span class="fa-li fa fa-envelope color-default"></span>office@escaperoomfactory.net</li>                         
                        		</ul>
                            </div>
                        </div>
                        <div class="white-space space-small"></div>
                    </div>
                    <div class="col-md-6">                    	<h3 class="fancy-title"><span>Contact Form</span></h3>						                        <!-- Form -->						<form class="form-horizontal" role="form" data-toggle="validator" id="sendMail" method="POST" enctype="multipart/form-data">                        	<div class="alert alert-danger" id="alert1">								<a href="#" class="close" data-dismiss="alert" aria-label="close"><h3 style="margin-top:-12px;color:#000;">&times;</h3></a>							  <strong><div id="status1"></div></strong>							</div>							<div class="alert alert-success" id="alert2">							<a href="#" class="close" data-dismiss="alert" aria-label="close"><h3 style="margin-top:-12px;color:#000;">&times;</h3></a>							  <strong><div id="status2"></div></strong>							</div>							<div class="form-group">								<div class="col-sm-6">      								<input type="text" class="form-control input-lg" id="inputName" name="inputName" placeholder="Enter Your Name" required>									<p class="help-block with-errors"></p>								</div>								<div class="col-sm-6">      								<input type="Email" class="form-control input-lg" id="inputEmail" name="inputEmail" placeholder="Enter Your Email" required>									<p class="help-block with-errors"></p>								</div>    								<div class="col-sm-12">									<input type="subject" class="form-control" id="inputSubject" placeholder="Subject" name="inputSubject" required>									<p class="help-block with-errors"></p>								</div> 								<div class="col-sm-12">      								<textarea class="form-control" rows="6" placeholder="Enter Your Message" id="inputMessage" name="inputMessage" required></textarea>									<p class="help-block with-errors"></p>								</div> 								<div class="col-sm-12">      								<button type="submit" class="btn btn-primary center-block" id="send" name="send">Send Message</button>								</div>							</div>						</form>                                                <!-- /Form -->                                                                        <div class="white-space space-small"></div>                    </div>
				</div> 
				<hr>
            </div>
			<!-- /Container -->
		</div>		<?php?>