<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Admin - Escape Room </title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url();?>css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo base_url();?>css/admin.css" rel="stylesheet">


    <!-- Custom Fonts -->
    <link href="http://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">Escape Room Factory</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
				<li>
                    <a href="<?php echo base_url("home");?>"><i class="fa fa-fw fa-globe"></i> Main Page</a>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> Admin <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="<?php echo base_url('login/logout');?>"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li class="active">
                        <a href="<?php echo base_url('admin');?>"><i class="fa fa-fw fa-dashboard"></i> Game Rooms</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h2 class="page-header">
                            Dashboard 
                        </h2>
						<a  data-toggle="modal" data-target="#myModal" class="pull-right btn btn-md btn-primary">+ Game Room</a>
                    </div>
                </div>
					</br>
				<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				  <div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">
					  <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="myModalLabel">Add Game Room</h4>
					  </div>
					  <div class="modal-body">
						<form role="form"  data-toggle="validator" id="addGameRoom" method="post" enctype="multipart/form-data">
							<div class="row">
								<div class="col-md-6 col-lg-6">
									<div class="form-group">
										<label>Room Name</label>
										<input type="text" name="name" class="form-control" required>
										<input type="hidden" id = "urlAdd" value="<?php echo base_url('admin');?>" name="url">
										<p class="help-block with-errors"></p>
									</div>
									<div class="form-group">
										<label>Price</label>
										<input type="text" name="price" class="form-control" required>
										<p class="help-block with-errors"></p>
									</div>
									<div class="form-group">
										<label>Difficulty</label>
										<input type="number" name="difficulty" min="1" max="10" required>
										<p class="help-block with-errors"></p>
									</div>
								</div>
								<div class="col-md-6 col-lg-6">
									<div class="form-group">
										<label>Story</label>
										<textarea class="form-control" name="story" rows="4" required></textarea>
										<p class="help-block with-errors"></p>
									</div>
									<div class="form-group">
										<label>Policy</label>
										<textarea class="form-control" name="policy" rows="4" required></textarea>
										<p class="help-block with-errors"></p>
									</div>
								</div>
							</div>
					  </div>
					  <div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">X</button>
						<button type="submit" class="btn btn-warning">SAVE</button>
					  </div>
					  </form>
					</div>
				  </div>
				</div>
				<div class="row">
                    <div class="col-lg-12 col-md-12">
						
                        <div class="table-responsive">
                            <table class="text-center table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th class="text-center">Room Name</th>
                                        <th class="text-center">Price</th>
                                        <th class="text-center">Main Picture</th>
                                        <th class="text-center">Background Picture</th>
                                        <th class="text-center">Attachments</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
								<?php foreach($room as $row) {?>	
                                    <tr>
                                        <td style="color:#f1c40f;"><?php echo $row->name;?></td>
                                        <td><?php echo $row->price;?></td>
										<?php if($row->main_picture == ""){?>
                                        <td>No Image&emsp;<a data-toggle="modal" data-target="#addMainPicture<?php echo $row->id;?>"  class="btn btn-primary" ><span class="fa fa-plus-circle"></span></a></td>
                                        <?php }else {?>
										<td><?php echo $row->main_picture;?> &emsp;<a data-toggle="modal" data-target="#editMainPicture<?php echo $row->id;?>" class="btn btn-warning" ><span class="fa fa-pencil-square-o"></span></a></td>
										<?php } ?>
										<?php if($row->bg_picture == ""){?>
                                        <td>No Image&emsp;<a data-toggle="modal" data-target="#addBackgroundPicture<?php echo $row->id;?>" class="btn btn-primary" ><span class="fa fa-plus-circle"></span></a></td>
                                        <?php }else {?>
										<td><?php echo $row->bg_picture;?> &emsp;<a data-toggle="modal" data-target="#editBgPicture<?php echo $row->id;?>" class="btn btn-warning" ><span class="fa fa-pencil-square-o"></span></a></td>
										<?php } ?>
                                        <td><a href="<?php echo base_url('admin/viewAttach/'.$row->id);?>" class="btn btn-md btn-primary">View <i class="fa fa-external-link" aria-hidden="true"></i></button></td>
                                        <td>
											<button type="button" data-toggle="modal" data-target="#editRoom<?php echo $row->id;?>" class="btn btn-md btn-warning"><span class="fa fa-cogs"></span></button>
											<button type="button" onclick="dltRoom(<?php echo $row->id; ?>)" class="btn btn-md btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
										</td>
                                    </tr>
									<div class="modal fade bs-example-modal-sm" id="deleteRoom" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
									  <div class="modal-dialog" role="document">
										<div class="modal-content">
										  <div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											<h4 class="modal-title" id="myModalLabel">Delete Game Room</h4>
										  </div>
										  <div class="modal-body text-center">
											<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>&emsp;
											<button type="button" id ="dltnow" class="btn btn-danger">DeleteNow</button>
										  </div>
										  <div class="modal-footer">
											
										  </div>
										</div>
									  </div>
									</div>	
									<div class="modal fade editRoom" id="editRoom<?php echo $row->id;?>"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
									  <div class="modal-dialog modal-lg" role="document">
										<div class="modal-content">
										  <div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											<h4 class="modal-title" id="myModalLabel">Edit Game Room</h4>
										  </div>
										  <div class="modal-body">
											<form role="form"  data-toggle="validator" id="editRoom" method="post" enctype="multipart/form-data">
												<div class="row">
													<div class="col-md-6 col-lg-6">
														<div class="form-group">
															<label>Room Name</label>
															<input type="text" value="<?php echo $row->name;?>" name="name" class="form-control" required>
															<input type="hidden" value="<?php echo $row->id;?>" name="id">
															<input type="hidden" id = "urlAdd" value="<?php echo base_url('admin');?>" name="url">
															<p class="help-block with-errors"></p>
														</div>
														<div class="form-group">
															<label>Price</label>
															<input type="text" value="<?php echo $row->price;?>" name="price" class="form-control" required>
															<p class="help-block with-errors"></p>
														</div>
														<div class="form-group">
															<label>Difficulty</label>
															<input type="number" value="<?php echo $row->difficulty;?>" name="difficulty" min="1" max="10" required>
															<p class="help-block with-errors"></p>
														</div>
													</div>
													<div class="col-md-6 col-lg-6">
														<div class="form-group">
															<label>Story</label>
															<textarea class="form-control" name="story" rows="4" required> <?php echo $row->story;?></textarea>
															<p class="help-block with-errors"></p>
														</div>
														<div class="form-group">
															<label>Policy</label>
															<textarea class="form-control" name="policy" rows="4" required> <?php echo $row->policy;?></textarea>
															<p class="help-block with-errors"></p>
														</div>
													</div>
												</div>
										  </div>
										  <div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">X</button>
											<button type="submit" class="btn btn-warning">SAVE</button>
										  </div>
										  </form>
										</div>
									  </div>
									</div>
									<div class="modal fade" id="addMainPicture<?php echo $row->id;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
									  <div class="modal-dialog modal-xs" role="document">
										<div class="modal-content">
										  <div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											<h4 class="modal-title" id="myModalLabel">Add Main Picture</h4>
										  </div>
										  <div class="modal-body">
											<form role="form"  data-toggle="validator" action="<?php echo base_url('admin/uploadMainPicture/'.$row->id);?>" method="post" enctype="multipart/form-data">
												<div class="row">
												
													<div class="col-md-4 col-lg-4 col-sm-4 col-xs-4"></div>
													<div class="col-md-4 col-lg-4 col-sm-4 col-xs-4">
														<div class="form-group">
															<label>Main Picture</label>
															<input type="file" name="main_picture" required>
															<p class="help-block with-errors"></p>
														</div>
													</div>
													<div class="col-md-4 col-lg-4 col-sm-4 col-xs-4"></div>
												</div>
										  </div>
										  <div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">X</button>
											<button type="submit" class="btn btn-warning">SAVE</button>
										  </div>
										  </form>
										</div>
									  </div>
									</div>
									<div class="modal fade" id="editMainPicture<?php echo $row->id;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
									  <div class="modal-dialog modal-xs" role="document">
										<div class="modal-content">
										  <div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											<h4 class="modal-title" id="myModalLabel">Edit Main Picture</h4>
										  </div>
										  <div class="modal-body">
											<form role="form"  data-toggle="validator" action="<?php echo base_url('admin/updateMainPicture/'.$row->id);?>" method="post" enctype="multipart/form-data">
												<div class="row">
												
													<div class="col-md-4 col-lg-4 col-sm-4 col-xs-4"></div>
													<div class="col-md-4 col-lg-4 col-sm-4 col-xs-4">
														<div class="form-group">
															<label>Main Picture</label>
															<input type="file" name="main_picture" required>
															<input type="hidden" value="<?php echo $row->main_picture;?>" name="old_main_picture" >
															<p class="help-block with-errors"></p>
														</div>
													</div>
													<div class="col-md-4 col-lg-4 col-sm-4 col-xs-4"></div>
												</div>
										  </div>
										  <div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">X</button>
											<button type="submit" class="btn btn-warning">SAVE</button>
										  </div>
										  </form>
										</div>
									  </div>
									</div>
									<div class="modal fade" id="editBgPicture<?php echo $row->id;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
									  <div class="modal-dialog modal-xs" role="document">
										<div class="modal-content">
										  <div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											<h4 class="modal-title" id="myModalLabel">Edit Background Picture</h4>
										  </div>
										  <div class="modal-body">
											<form role="form"  data-toggle="validator" action="<?php echo base_url('admin/updateBgPicture/'.$row->id);?>" method="post" enctype="multipart/form-data">
												<div class="row">
												
													<div class="col-md-4 col-lg-4 col-sm-4 col-xs-4"></div>
													<div class="col-md-4 col-lg-4 col-sm-4 col-xs-4">
														<div class="form-group">
															<label>Background Picture</label>
															<input type="file" name="bg_picture" required>
															<input type="hidden" value="<?php echo $row->bg_picture;?>" name="old_bg_picture" >
															<p class="help-block with-errors"></p>
														</div>
													</div>
													<div class="col-md-4 col-lg-4 col-sm-4 col-xs-4"></div>
												</div>
										  </div>
										  <div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">X</button>
											<button type="submit" class="btn btn-warning">SAVE</button>
										  </div>
										  </form>
										</div>
									  </div>
									</div>
				<div class="modal fade" id="addBackgroundPicture<?php echo $row->id;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				  <div class="modal-dialog modal-xs" role="document">
					<div class="modal-content">
					  <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="myModalLabel">Add Background Picture</h4>
					  </div>
					  <div class="modal-body">
						<form role="form"  data-toggle="validator" action="<?php echo base_url('admin/uploadBgPicture/'.$row->id);?>" method="post" enctype="multipart/form-data">
							<div class="row">
								<div class="col-md-4 col-lg-4 col-sm-4 col-xs-4"></div>
								<div class="col-md-4 col-lg-4 col-sm-4 col-xs-4">
									<div class="form-group">
										<label>Background Picture</label>
										<input type="file" name="bg_picture" required>
										<p class="help-block with-errors"></p>
									</div>
								</div>
								<div class="col-md-4 col-lg-4 col-sm-4 col-xs-4"></div>
							</div>
					  </div>
					  <div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">X</button>
						<button type="submit" class="btn btn-warning">SAVE</button>
					  </div>
					  </form>
					</div>
				  </div>
				</div>
								<?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->
 <!--Modal Message-->
		<div class="modal fade" id="noti_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-sm">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
						<center><h4 class="modal-title" id="modal_message"></h4></center>
					</div>
				</div>
			</div>
		</div>   
    </div>
    <!-- /#wrapper -->
				
	<div class="modal fade" id="statusModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<br>
		  </div>
		  <div class="modal-body">
			<center><h3 id="txtStatus"></h3></center>
		  </div>
		  <div class="modal-footer">
		  </div>
		</div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
    <script src="<?php echo base_url();?>js/jquery-1.11.1.min.js"></script>
    <script src="<?php echo base_url();?>js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>js/validator.js"></script>
<script type="text/javascript">
function modal_message(message) {
		document.getElementById('modal_message').innerHTML = message;
		$("#noti_modal").modal('show');	
	}
	
	function reload(){
		
		location.reload();
	}
		function dltRoom(id){
		console.log(id);
		$("#deleteRoom").modal('show');
		 $("#dltnow").click(function(){
		  $.get('admin/deleteRoom'+'/'+id, {},function(data) {
			
				$("#deleteRoom").modal('hide');
			
				setInterval(reload, 1500);
				console.log(data);
            }, "json");
		
		  });
	}
		(function($){
			var getModal;
			$("#addGameRoom").submit(function(e) {
				e.preventDefault();
			  	$.ajax({
				type: "POST",
				url: "admin/addRoom",
				dataType: "json",
				data:  $(this).serialize() ,
				success:
						  function(data) {
							   var getUrl =  $("#urlAdd").attr("value");
								if(data == "true"){
									$("#txtStatus").html("Successfully Added.");
									$('#statusModal').modal('show');
									$('#myModal').modal('hide');
									setTimeout(function(){ location.href = getUrl; }, 2000);
								}
						  else{
							  $("#txtStatus").html("Error.");
									$('#statusModal').modal('show');
									$('#myModal').modal('hide');
									setTimeout(function(){ location.href = getUrl; }, 2000);
						  }
					
							
						  },
					error:
						function(data){
								  $("#txtStatus").html("Error.");
									$('#statusModal').modal('show');
									$('#myModal').modal('hide');
						}
				});
			});
			
			$("#editRoom").submit(function(e) {
				e.preventDefault();
			  	$.ajax({
				type: "POST",
				url: "admin/updateRoom",
				dataType: "json",
				data:  $(this).serialize() ,
				success:
						  function(data) {
							  getModal =  $(".editRoom").attr("id");
							  var getUrl =  $("#urlAdd").attr("value");
							  var modalName = "#"+getModal;
								if(data == "true"){
									$("#txtStatus").html("Successfully Updated.");
									$('#statusModal').modal('show');
									$(".editRoom").modal('hide');
									
									setTimeout(function(){ location.href = getUrl; }, 2000);
								}
						  else{
							  $("#txtStatus").html("Error.");
									$('#statusModal').modal('show');
									$(".editRoom").modal('hide');
									setTimeout(function(){ location.href = getUrl; }, 2000);
						  }
					
							
						  },
					error:
						function(data){
								  $("#txtStatus").html("Error.");
									$('#statusModal').modal('show');
									$('#myModal').modal('hide');
						}
				});
			});
		})(jQuery);
	
		</script>

</body>

</html>
