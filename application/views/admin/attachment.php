<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Admin - Escape Room </title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url();?>css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo base_url();?>css/admin.css" rel="stylesheet">


    <!-- Custom Fonts -->
    <link href="http://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">Escape Room Factory</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
				<li>
                    <a href="<?php echo base_url("home");?>"><i class="fa fa-fw fa-globe"></i> Main Page</a>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> Admin <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="<?php echo base_url('login/logout');?>"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li class="active">
                        <a href="<?php echo base_url('admin');?>"><i class="fa fa-fw fa-dashboard"></i> Game Rooms</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h2 class="page-header">
                            Attachments 
                        </h2>

                    </div>
                </div>
					</br>
				<div class="row">
                    <div class="col-lg-8 col-md-8">
						
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th >Attachment Name</th>
                                        <th class="text-right">Remove</th>
                                    </tr>
                                </thead>
                                <tbody>
								<?php if($attach){?>
									<?php foreach($attach as $row) {?>	
										<tr>
											<td ><?php echo $row->name;?></td>
											<td ><button type="button" onclick="dltRoom(<?php echo $row->id; ?>,<?php echo $id; ?>)" class="pull-right btn btn-md btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i></button></td>
										</tr>
									<?php } ?>
								<?php }else{ ?>
									<tr>
											<td >No Attachment(s).</td>
											<td ></td>
										</tr>
								<?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
					<div class="col-lg-4 col-md-4">

						<form role="form"  data-toggle="validator" action="<?php echo base_url('admin/attachFile');?>" method="post" enctype="multipart/form-data">
						<input type="file" multiple name="userfile[]" size="100" required>
						<p class="help-block with-errors"></p>
						<input type="hidden" name="id" value="<?php echo $id;?>" required>
						<br/>
						<div class="row">
							<div class="col-lg-4 col-md-4"></div>
							<div class="col-lg-4 col-md-4">	
								<input type="submit" class="pull-center btn btn-md btn-primary" value="Upload" />
							</div>
							<div class="col-lg-4 col-md-4"></div>
							
						</div>
						</form>
					</div>
                </div>

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->
   
    </div>
    <!-- /#wrapper -->
<div class="modal fade bs-example-modal-sm" id="deleteRoom" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
									  <div class="modal-dialog" role="document">
										<div class="modal-content">
										  <div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											<h4 class="modal-title" id="myModalLabel">Delete Attachment</h4>
										  </div>
										  <div class="modal-body text-center">
											<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>&emsp;
											<button type="button" id ="dltnow" class="btn btn-danger">DeleteNow</button>
										  </div>
										  <div class="modal-footer">
											
										  </div>
										</div>
									  </div>
									</div>	
    <script src="<?php echo base_url();?>js/jquery-1.11.1.min.js"></script>
    <script src="<?php echo base_url();?>js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>js/validator.js"></script>
<script type="text/javascript">
	function reload(){
		
		location.reload();
	}
		function dltRoom(id,game_id){
		$("#deleteRoom").modal('show');
		 $("#dltnow").click(function(){
		  $.get('../deleteAttachment'+'/'+id+'/'+game_id, {},function(data) {
			
				$("#deleteRoom").modal('hide');
			
				setInterval(reload, 1200);
				console.log(data);
            }, "json");
		
		  });
	}
	</script>
</body>

</html>
