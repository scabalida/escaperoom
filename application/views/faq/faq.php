 <div class="main-wrapper-header fancy-header dark-header" data-stellar-background-ratio="0.4">
			
            <div class="bg-overlay bg-overlay-gdark"></div>

 			<div class="container">
            
				<div class="row">
                	<div class="col-sm-12 columns">
                		<div class="page-title">
                    		<h2>FAQ</h2>                    
                    	</div>
                    	<div class="breadcrumbs-wrapper">               
							<ol class="breadcrumb">
  								<li><a href="#">Home</a></li>
								<li class="active">FAQ </li>
							</ol>
                		</div>
					</div>
				</div>
                
			</div>
 
        </div>

		<!-- Main Container -->
		<div class="main-wrapper">

        	<!-- Container -->
            <div class="container">
            	<div class="white-space space-big"></div>


            	<div id="grid-containter">
        <div class="card-grid" style="perspective: 200px; position: relative; transform-style: preserve-3d;">
          <div class="front" style="font-size:20px;padding-top:70px;backface-visibility: hidden; transform-style: preserve-3d; position: absolute; z-index: 1; height: 100%; width: 100%; transition: all 0.5s ease-out; transform: rotateY(0deg);">			What do I get when I buy a room?		  </div>
          <div class="back" style="text-align:left;transform: rotateY(-180deg); z-index: 0; backface-visibility: hidden; transform-style: preserve-3d; position: absolute; height: 100%; width: 100%; transition: all 0.5s ease-out;">		  <p>-You get few PDF documents</p>		  <p>-Introduction Story</p>		  <p>-Bill Of Materials – all that You need for this game to be set up (in detail description and reference picture)</p>		  <p>-NARRATIVE STORY</p>		  <p>-Gameplay Diagram and step by step instructions</p>		  <p>-Room Reset Instruction (for game master)</p>		  <p>-Hints</p>		  </div>
        </div>
        <div class="card-grid" style="perspective: 200px; position: relative; transform-style: preserve-3d;">
          <div class="front" style="font-size:20px;padding-top:70px; backface-visibility: hidden; transform-style: preserve-3d; position: absolute; z-index: 1; height: 100%; width: 100%; transition: all 0.5s ease-out; transform: rotateY(0deg);">		  How many players are suppose to play the game?		 		  </div>
          <div class="back" style="transform: rotateY(-180deg); z-index: 0; backface-visibility: hidden; transform-style: preserve-3d; position: absolute; height: 100%; width: 100%; transition: all 0.5s ease-out;">For every room there is a recommended number of players, there is no exact rule.</div>
        </div>
        <div class="card-grid" style="perspective: 200px; position: relative; transform-style: preserve-3d;">
          <div class="front" style="font-size:20px;padding-top:70px;backface-visibility: hidden; transform-style: preserve-3d; position: absolute; z-index: 1; height: 100%; width: 100%; transition: all 0.5s ease-out; transform: rotateY(0deg);">What are electric props?</div>
          <div class="back" style="transform: rotateY(-180deg); z-index: 0; backface-visibility: hidden; transform-style: preserve-3d; position: absolute; height: 100%; width: 100%; transition: all 0.5s ease-out;">Pre-build props that give the live escape game that shine. We have partnership with 1987shop and if You buy our game You get 20% discount and 24/7 customer support on setting up the props.</div>
        </div>
        <div class="card-grid" style="perspective: 200px; position: relative; transform-style: preserve-3d;">
          <div class="front" style="font-size:20px;padding-top:70px;backface-visibility: hidden; transform-style: preserve-3d; position: absolute; z-index: 1; height: 100%; width: 100%; transition: all 0.5s ease-out; transform: rotateY(0deg);">Are these games hard to set up?</div>
          <div class="back" style="transform: rotateY(-180deg); z-index: 0; backface-visibility: hidden; transform-style: preserve-3d; position: absolute; height: 100%; width: 100%; transition: all 0.5s ease-out;">Absolutly NOT! All the items are easy to find. Items that might not be available everywhere we have put a link on where to buy it. </div>
        </div>
        <div class="card-grid" style="perspective: 200px; position: relative; transform-style: preserve-3d;">
          <div class="front" style="font-size:20px;padding-top:70px;backface-visibility: hidden; transform-style: preserve-3d; position: absolute; z-index: 1; height: 100%; width: 100%; transition: all 0.5s ease-out; transform: rotateY(0deg);">I have a 3 bedroom place, but in the game it says 4 room needed. What should I do?</div>
          <div class="back" style="transform: rotateY(-180deg); z-index: 0; backface-visibility: hidden; transform-style: preserve-3d; position: absolute; height: 100%; width: 100%; transition: all 0.5s ease-out;">No worries, just contact us and we will change the gameplay so it fits Your needs.</div>
        </div>
        <div class="card-grid" style="perspective: 200px; position: relative; transform-style: preserve-3d;">
          <div class="front" style="font-size:20px;padding-top:70px;backface-visibility: hidden; transform-style: preserve-3d; position: absolute; z-index: 1; height: 100%; width: 100%; transition: all 0.5s ease-out; transform: rotateY(0deg);">I would like a custom designed room?</div>
          <div class="back" style="transform: rotateY(-180deg); z-index: 0; backface-visibility: hidden; transform-style: preserve-3d; position: absolute; height: 100%; width: 100%; transition: all 0.5s ease-out;">Just contact us and we will make it happen!</div>
        </div>
        <div class="card-grid" style="perspective: 200px; position: relative; transform-style: preserve-3d;">
          <div class="front" style="font-size:20px;padding-top:70px;backface-visibility: hidden; transform-style: preserve-3d; position: absolute; z-index: 1; height: 100%; width: 100%; transition: all 0.5s ease-out; transform: rotateY(0deg);">Why should I buy a room?</div>
          <div class="back" style="transform: rotateY(-180deg); z-index: 1; backface-visibility: hidden; transform-style: preserve-3d; position: absolute; height: 100%; width: 100%; transition: all 0.5s ease-out;">Live Escape Game has become very popular and it’s growing day by day. It is a cheap, easy to set up business and it is SO MUCH FUN!</div>
        </div>
      </div>
				<hr>
            </div>
			<!-- /Container -->
                                   
		</div>
		<!-- /Main Container -->   