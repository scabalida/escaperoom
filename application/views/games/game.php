 <div class="main-wrapper-header fancy-header dark-header" data-stellar-background-ratio="0.4">
			
            <div class="bg-overlay bg-overlay-gdark"></div>

 			<div class="container">
            
				<div class="row">
                	<div class="col-sm-12 columns">
                		<div class="page-title">
                    		<h2>Games</h2>                    
                    	</div>
                    	<div class="breadcrumbs-wrapper">               
							<ol class="breadcrumb">
  								<li><a href="#">Home</a></li>
								<li class="active">Games </li>
							</ol>
                		</div>
					</div>
				</div>
                
			</div>
 
        </div>  
<!-- Main Container -->
		<div class="main-wrapper" >
			
        	<!-- Container -->
            <div class="container">
				<div class="row">

                    <!-- Content -->
					<div class="col-md-12">
						
						<h2 class="fancy-title text-center"><span>&nbsp;</span></h2>
						<div class="white-space space-small"></div>
						<div class="row">
							<?php if($room != NULL){?>
								<?php foreach($room as $row) {?>
									<div class="col-md-3">
											<!-- Shop Product -->
											
											<div class="shop-product" >
												<a href="<?php echo base_url('games/info/'.$row->id);?>">
												<!-- Overlay Img -->
												<div class="overlay-wrapper">
													<?php if($row->main_picture != NULL){?>
														<img src="<?php echo base_url('upload/'.$row->main_picture);?>" alt="Product 1">
													<?php }else{ ?>
														<img src="<?php echo base_url('img/no_img.jpg');?>" alt="Product 1">
													<?php } ?>
													<div class="rating"  style="direction:ltr">
														<span><?php echo $row->name;?></span>
													</div>
												</div>
												<!-- Overlay Img -->
												<div class="shop-product-info">
													<p class="product-price">$ <?php echo $row->price;?></p>
												</div>
												</a>
											</div>
											
											<!-- /Shop Product -->
										<div class="white-space space-small"></div>
									</div>
								<?php } ?>
							<?php } else{ ?>
									<div class="col-md-3">
											<h3>No Data.</h3>
											</a>
											<!-- /Shop Product -->
										<div class="white-space space-small"></div>
									</div>
							<?php } ?>
							
						</div>
						<hr>  
						
                    </div>
					<!-- /Content -->
				
                </div>
            </div>
			<!-- /Container -->
                                   
		</div>
	