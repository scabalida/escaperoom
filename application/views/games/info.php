 <div class="main-wrapper-header fancy-header dark-header" data-stellar-background-ratio="0.4">
			
            <div class="bg-overlay bg-overlay-gdark"></div>

 			<div class="container">
            
				<div class="row">
                	<div class="col-sm-12 columns">
                		<div class="page-title">
                    		<h2><?php echo ucwords($info->name);?></h2>                    
                    	</div>
                    	<div class="breadcrumbs-wrapper">               
							<ol class="breadcrumb">
  								<li><a href="#">Games</a></li>
								<li class="active"><?php echo ucwords($info->name);?></li>
							</ol>
                		</div>
					</div>
				</div>
                
			</div>
 
        </div>  
					 
		<div class="main-wrapper" id="main_wrap">
			
        	<!-- Container -->
            <div class="container">
            	<div class="row"> 
                    <div class="col-sm-6">
						<div class="white-space space-small"></div>
                    	<div class="white-space space-small"></div>
						<?php if($info->main_picture != NULL){?>
								<img src="<?php echo base_url('upload/'.$info->main_picture);?>" style="width:500px; height:400px;" class="img-responsive">
						<?php }else{ ?>
								<img src="<?php echo base_url('img/no_img.jpg');?>" style="width:500px; height:400px;" class="img-responsive">
						<?php } ?>
						<img id="bg_pic" src="<?php echo base_url('upload/'.$info->bg_picture);?>" style="display:none !important" class="img-responsive">
						<img id="get_pic" src="<?php echo $info->bg_picture;?>" style="display:none !important" class="img-responsive">
						<div class="white-space space-small"></div>
						<a id="buynow" class="btn btn-warning btn-block btn-alt margin-bottom10">Buy Now</a>

						<div class="white-space space-small"></div>
                    </div>
                	<div class="col-sm-6" id="infoPage">
                    	<div class="white-space space-small"></div>
                    	<h4 class="fancy-title"><span>$ <?php echo $info->price;?></span></h4>
						<p style="color:#fff">Introduction: <?php echo $info->story;?></p>       
						<p style="color:#fff">Policies: <?php echo $info->policy;?></p> 
						<div class="progress">
  							<div class="progress-bar animation fadeInLeft" role="progressbar" aria-valuenow="<?php echo $info->difficulty;?>" aria-valuemin="0" aria-valuemax="10" style="width: 90%;">
    							<span class="pb-content-text">GAME DIFFICULTY <?php echo ($info->difficulty / 10) * 100;?> %</span>
  							</div>
						</div>
					</div>
					<div class="col-sm-6" id="checkoutPage" style="color:#fff">
                    	<div class="white-space space-small"></div>
                    	<div class="white-space space-small"></div>
						<h4 class="fancy-title" style="margin-bottom:20px"><span>Checkout Now</span></h4>
						<div class="alert alert-danger" id="alert1">
								<a href="#" class="close" data-dismiss="alert" aria-label="close"><h3 style="margin-top:-12px;color:#000;">&times;</h3></a>
							  <strong><div id="status1"></div></strong>
							</div>
						<div class="alert alert-success" id="alert2">
								<a href="#" class="close" data-dismiss="alert" aria-label="close"><h3 style="margin-top:-12px;color:#000;">&times;</h3></a>
							  <strong><div id="status2"></div></strong>
							</div>
						<i class="fa fa-arrow-left" style="cursor:pointer" id="backToInfo" aria-hidden="true"> Back</i>
						<br/>
						<br/>
						<form role="form"  data-toggle="validator" id="paynow" method="post" enctype="multipart/form-data">
							<div class="row">
								<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
									<div class="form-group">
										<label>Full Name</label>
										<input type="text" id="name" name="name" class="form-control" style="color:#fff; border:1px solid #fff" required>
										<input type="hidden" name="id" value="<?php echo $info->id;?>">
										<p class="help-block with-errors"></p>
									</div>
									<div class="form-group">
										<label>Email Address</label>
										<input type="email" id="email" name="email" class="form-control" style="color:#fff ; border:1px solid #fff" required>
										<p class="help-block with-errors"></p>
									</div>
									<div class="form-group">
										<label>Contact Number</label>
										<input type="text" id="contact" name="contact" class="form-control" style="color:#fff; border:1px solid #fff" required>
										<p class="help-block with-errors"></p>
									</div>
									<div class="form-group">
										<label>Home Address</label>
										<textarea id="address" class="form-control" name="address" rows="3" style="color:#fff; border:1px solid #fff" required></textarea>
										<p class="help-block with-errors"></p>
									</div>
								</div>
							</div>
							<!-- PayPal Logo --><table border="0" cellpadding="10" cellspacing="0" align="center"><tr><td align="center"></td></tr><tr><td align="center"><button style="background:transparent;border:0" type="submit" ><img class="img-responsive" src="https://www.paypalobjects.com/webstatic/en_US/i/buttons/buy-logo-large.png" alt="Buy now with PayPal" /></button></td></tr></table><!-- PayPal Logo -->
					</form>
					</div>
                        <div class="white-space space-small"></div>
                </div>
			</div>
		
				<hr>
				
        </div>
			<!-- /Container -->
                            
	
		<!-- /Main Container -->   