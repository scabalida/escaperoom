<!-- Main Wrapper Header -->
        <div class="main-wrapper-header fancy-header dark-header" data-stellar-background-ratio="0.4">
			
            <div class="bg-overlay bg-overlay-gdark"></div>

 			<div class="container">
            
				<div class="row">
                	<div class="col-sm-12 columns">
                		<div class="page-title">
                    		<h2>Game Policies</h2>                    
                    	</div>
                    	<div class="breadcrumbs-wrapper">               
							<ol class="breadcrumb">
  								<li><a href="#">Home</a></li>
								<li class="active">Policies</li>
							</ol>
                		</div>
					</div>
				</div>
                
			</div>
 
        </div>       
		<!-- /Main Wrapper Header -->
		<div class="main-wrapper">

        	<!-- Container -->
            <div class="container">
                
            	<div class="row">
                    <div class="row">
                    <div class="col-m-5 col-sm-5">
                    	<div class="white-space space-small"></div>
						<h4 class="fancy-title animation fadeInLeft"><span>Games</span></h4>
						<?php foreach($room as $row) {?>
							<button type="button btn-margin" onclick="clickPolicy(<?php echo $row->id;?>)" class="btn btn-warning btn-alt margin-bottom10"><?php echo $row->name;?></button>
							
						<?php } ?>
                    </div>
                	<div class="col-m-7 col-sm-7">
                    	<div class="white-space space-small"></div>
                    	<h4 class="fancy-title animation fadeInRight"><span>Policy</span></h4>
						<p class="animation fadeInRight"></p>                        
							<div>
								<p id="game_name"></p> 
								<p id="game_policy"></p> 
                            </div>
							
                        <div class="white-space space-small"></div>
                    </div>
				</div>
				</div>
            	
				<hr>
            </div>
			
		</div>