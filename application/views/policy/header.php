<!DOCTYPE html>
<html lang="en" >
<head>
	<meta charset="utf-8">	
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<title>Escape Room</title>

    <!-- Styles -->
           
		<!-- Bootstrap core CSS -->
  		<link rel="stylesheet" href="css/bootstrap.css">        

		<!-- Bootstrap RTL 
  		<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-rtl/3.2.0-rc2/css/bootstrap-rtl.css"> -->

		<!-- Font Awesome 4.1.0 -->
		<link href="http://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">

		<!-- Escape Icons Font -->
		<link href="<?php echo base_url();?>css/escape-font-styles.css" rel="stylesheet">

		<!-- SLIDER REVOLUTION 4.x CSS SETTINGS -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/revolutionslider/settings.css" media="screen" />
    	
    	<!-- Animate CSS -->       
    	<link href="<?php echo base_url();?>css/themecss/animate.css" rel="stylesheet">

    	<!-- Lightbox CSS -->       
    	<link href="<?php echo base_url();?>css/themecss/lightbox.css" rel="stylesheet">
              
    	<!-- OWL Carousel -->       
    	<link href="<?php echo base_url();?>css/owl-carousel/owl.carousel.css" rel="stylesheet">
        <link href="<?php echo base_url();?>css/owl-carousel/owl.transitions.css" rel="stylesheet">

		<!-- Theme CSS -->
  		<link href="<?php echo base_url();?>css/escape-dark.css" rel="stylesheet">
        
		<!-- Color CSS -->
		<link href="<?php echo base_url();?>css/colors/color-default.css" rel="stylesheet" title="style1">
   
	    <!-- Google Fonts -->
	    <link href='http://fonts.googleapis.com/css?family=Raleway:400,200,300,100,500,600,700,800,900' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Playball' rel='stylesheet' type='text/css'>
    
</head>
<body class="bg-custom" data-spy="scroll" data-target=".navbar">
<div id="preloader"></div>
    
	<!-- Page Main Wrapper -->
	<div class="page-wrapper boxed" id="page-top">

		<!-- Header Container -->
		<div class="header-wrapper">
			<div class="header-top">

                <!-- Container -->
                <div class="container">
                    
                    	<div class="row">
                            <div class="col-md-9 col-sm-9 columns"></div>
                            <!-- Right -->
                            <div class="col-md-3 col-sm-3 columns">
                            	<div class="header-top-right">
								<!-- Right Menu -->
									<ul class="top-menu">
										<?php if($this->session->userdata('loggedin') == TRUE){ ?>
										<li><a href="<?php echo base_url('admin');?>" ><i class="fa fa-cog"></i>Admin Panel</a></li>
										<li><a href="<?php echo base_url('login/logout');?>" ><i class="fa fa-power-off" aria-hidden="true"></i>Sign Out</a></li>
										<?php } else{ ?>
										<li><a href="<?php echo base_url('login');?>" ><i class="fa fa-user"></i>Sign In</a></li>
										<?php }?>
									</ul>
								<!-- /Right Menu -->
                                </div>
                            </div>
                            <!-- /right -->
                        
                        </div>
                
                </div>
				<!-- /Container -->
   
		    </div>   
			<!-- Header Main Container -->
   			<div class="header-main">

                <!-- Container -->
                <div class="container">
                    
                	<!-- Main Navigation & Logo -->                    
					<div class="main-navigation">
                    
                    	<div class="row">
                            
                            <!-- Main Navigation -->
                            <div class="col-md-12 columns">

								<nav class="navbar navbar-default gfx-mega nav-left" role="navigation">
									
                                    	
										<!-- Brand and toggle get grouped for better mobile display -->
										<div class="navbar-header">
											<a class="navbar-toggle" data-toggle="collapse" data-target="#gfx-collapse"></a>
                            				<div class="logo">
                           	    				<a href="<?php echo base_url('home');?>"><img src="<?php echo base_url();?>img/theme/logo.jpg" alt="Logo"></a>
                                			</div>
										</div>

										<!-- Collect the nav links, forms, and other content for toggling -->
										<div class="collapse navbar-collapse" id="gfx-collapse">
											<ul class="nav navbar-nav gfx-nav">
												<li class="dropdown gfx-mega-fw"><a href="<?php echo base_url('home');?>" >Home</a>
												<li class="dropdown gfx-mega-fw"><a href="<?php echo base_url('about');?>" >About Us</a>
												<li class="dropdown gfx-mega-fw"><a href="<?php echo base_url('games');?>" >Games</a>
												<li class="dropdown gfx-mega-fw"><a href="<?php echo base_url('faq');?>" >FAQ</a>
												<li class="active gfx-mega-fw"><a href="<?php echo base_url('policy');?>" >Policies</a>
												<li class="dropdown gfx-mega-fw"><a href="<?php echo base_url('contact');?>" >Contact Us</a>
											</ul>
										</div><!-- /.navbar-collapse -->
									
								</nav>

                            </div>
                            <!-- /Main Navigation -->
                        
                        </div>              
                    
					</div>
                    <!-- /Main Navigation & Logo -->
                
                </div>
				<!-- /Container -->
   
		    </div>   
			<!-- /Header Main Container -->

		</div>