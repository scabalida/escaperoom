		
		<!-- Footer Container -->
		<div class="footer-wrapper">
			
			<!-- Footer Bottom Container -->
			<div class="footer-bottom">

                <!-- Container -->
                <div class="container">
					
                	<div class="row">
                        <!-- Footer Menu -->
  						<div class="col-md-12 col-sm-12 columns">
                        	<div class="menu-footer">
                            	<ul class="list-inline">
                                	<li><a href="<?php echo base_url('home');?>">Home</a></li>
                                    <li><a href="<?php echo base_url('about');?>">About Us</a></li>
                                    <li><a href="<?php echo base_url('games');?>">Games</a></li>
                                    <li><a href="<?php echo base_url('faq');?>">FAQ</a></li>
                                    <li><a href="<?php echo base_url('policy');?>">Policies</a></li>                                                                        
                                    <li><a href="<?php echo base_url('contact');?>">Contact Us</a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- /Footer Menu -->
                    	<!-- Copyright -->
  						<div class="col-md-12 col-sm-12 columns">
                        	<div class="copyright">
                        		<p>Copyright © 2016 - EscapeRoom | All Rights Reserved</p>
                            </div>
                        </div>
                        <!-- /Copyright -->
                        <!-- Footer Social -->
  						<div class="col-md-12 col-sm-12 columns">
                        	<div class="social-footer">
                            	<ul class="list-inline social-list">
                                	<li><a href="" class="ToolTip" title="Facebook"><span class="fa fa-facebook"></span></a></li>
                                    <li><a href="" class="ToolTip" title="Twitter"><span class="fa fa-twitter"></span></a></li>
                                    <li><a href="" class="ToolTip" title="Google+"><span class="fa fa-google-plus"></span></a></li>
                                    <li><a href="" class="ToolTip" title="Pinterest"><span class="fa fa-pinterest"></span></a></li>
                                    <li><a href="" class="ToolTip" title="Linkedin"><span class="fa fa-linkedin"></span></a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- /Footer Social -->
					</div>
                
                </div>
				<!-- /Container -->

			</div>
			<!-- /Footer Bottom Container -->

		</div>
		<!-- /Footer Container -->

	</div>	

	<!-- Back To Top -->
	<a href="#page-top" class="scrollup smooth-scroll" ><span class="fa fa-angle-up"></span></a>
	<!-- /Back To Top -->

 
    <!-- Javascripts
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
     <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
	
	<script>
	$(document).ready(function() {
		
	
  });
	function clickPolicy(id){
		
	
		   window.jQuery.get('admin/getPolicy'+'/'+id, {},function(data) {
				
				window.jQuery("#game_name").text(data.name+" Policy");
				window.jQuery("#game_policy").text(data.policy);
				console.log(data.name);
				console.log(data.policy);
            }, "json");
		
		
	}
	
	
   

		
	</script>
    <script src="<?php echo base_url();?>js/bootstrap.min.js"></script>

	<script type="text/javascript" src="<?php echo base_url();?>js/themejs/jquery.countdown.js"></script>

	<!-- Preloader -->
	<script src="<?php echo base_url();?>js/themejs/jquery.queryloader2.min.js" type="text/javascript"></script>

	<!-- Smooth Scroll -->
	<script src="<?php echo base_url();?>js/themejs/SmoothScroll.js" type="text/javascript"></script>

	<!-- Stick On Scroll -->
	<script src="<?php echo base_url();?>js/themejs/jquery.stickOnScroll.js" type="text/javascript"></script>
    
    <!-- Scrolling Smooth to section - requires jQuery Easing plugin -->
	<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>

    <!-- SLIDER REVOLUTION 4.x SCRIPTS --> 
    <script src="<?php echo base_url();?>js/revolutionslider/jquery.themepunch.plugins.min.js"></script>  
    <script src="<?php echo base_url();?>js/revolutionslider/jquery.themepunch.revolution.min.js"></script>       

	<!-- LivIcons-->
	<script src="<?php echo base_url();?>js/livicons/livicons-1.3.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>js/livicons/raphael-min.js" type="text/javascript"></script> 
    
	<!-- Portfolio -->
	<script src="<?php echo base_url();?>js/themejs/jquery.isotope.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>js/themejs/jquery.colio.min.js" type="text/javascript"></script>

	<!-- Parallax -->
	<script src="<?php echo base_url();?>js/themejs/jquery.stellar.min.js" type="text/javascript"></script>

	<!-- Carousel -->
	<script src="<?php echo base_url();?>js/owl-carousel/owl.carousel.js" type="text/javascript"></script>

	<!-- Counters -->
	<script src="<?php echo base_url();?>js/themejs/jquery.countTo.js" type="text/javascript"></script>

	<!-- Lightbox -->
	<script src="<?php echo base_url();?>js/themejs/jquery.magnific-popup.min.js" type="text/javascript"></script>

	<!-- Tooltips -->
	<script src="<?php echo base_url();?>js/themejs/jQuery.Opie.Tooltip.min.js" type="text/javascript"></script>

	<!-- Animation Viewport -->
	<script src="<?php echo base_url();?>js/themejs/jquery.waypoints.min.js" type="text/javascript"></script>
    
	<!-- Pie Chart -->
	<script src="<?php echo base_url();?>js/themejs/jquery.easypiechart.min.js" type="text/javascript"></script>
 
 	<!-- Twitter -->
	<script src="<?php echo base_url();?>twitter/jquery.tweet.js" type="text/javascript"></script>
    
   
    <!-- Load Scripts -->
	<script src="<?php echo base_url();?>js/themejs/application.js"></script>
	
  </body>
</html>