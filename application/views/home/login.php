	<div class="main-wrapper">
        	<!-- Container -->
        <div class="container">
			
            <div class="white-space space-big"></div>
			<div class="row">
				<div class="col-lg-4 col-md-4 col-sm-4"></div>
				<div class="col-lg-4 col-md-4 col-sm-4" id="loginF">
					<h3 class="fancy-title text-center"><span>Login Form</span></h3>
						
                        <!-- Form -->
						<form class="form-horizontal"  data-toggle="validator"  id="loginForm" role="form" method="POST">
                        	<div id="status" >
								<div class="success">
								<div class="alert alert-danger fade in"><a class="close" aria-hidden="true" data-dismiss="alert">
									<i class="fa fa-times"></i></a> Login Failed 
									</div>
								</div>
							</div>
							<div class="form-group has-feedback">
								<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="input-group">
									  <span class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></span>
									  <input type="text" class="form-control" id="inputUsername" placeholder="Enter your Username" name="inputUsername" required>
									  <input type="hidden" value="<?php echo base_url("home");?>"  id="url" name="url">
									</div>
									<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
									<div class="help-block with-errors"></div>
								</div>
							</div>
							<div class="form-group has-feedback">
								<div class="col-md-12 col-sm-12 col-xs-12">
									<div class="input-group">
									  <span class="input-group-addon"><i class="fa fa-unlock-alt" aria-hidden="true"></i></span>
									  <input type="password" class="form-control" id="inputPassword" placeholder="Enter your Password" name="inputPassword" required>
									</div>
									<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
									<div class="help-block with-errors"></div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12 col-sm-12 col-xs-12">
      								<button type="submit" class="btn btn-primary btn-block" id="send" name="send">Sign In</button>
								</div>
							</div>
						</form>                        
                        <!-- /Form --> 
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4"></div>
			</div>
		
            <div class="white-space space-big"></div>
				<hr>
		</div>
	</div>
				