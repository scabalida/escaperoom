		<!-- Slider Container -->
		<div class="slider-wrapper">


	<!-- START REVOLUTION SLIDER 3.1 rev5 fullwidth mode -->

	<div class="tp-banner-container">
		<div class="tp-banner" >
			<ul>
				
				<!-- SLIDE  -->
				<li data-transition="slidehorizontal" data-slotamount="1" data-masterspeed="1000" data-fstransition="fade" data-fsmasterspeed="1000" data-fsslotamount="2" >
					<!-- MAIN IMAGE -->
					<img src="<?php echo base_url();?>img/Slide2.jpg" alt="slidebg1"  data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">

					<div class="tp-caption large_bold_white customin customout tp-resizeme rs-parallaxlevel-3"
						data-x="center"
						data-y="150"

						data-splitin="words"
						data-elementdelay="0.1"
						data-start="1250"
						data-speed="1500"
						data-easing="Power3.easeOut"
						data-customin="x:0;y:-20;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"

						data-splitout="lines"
						data-endelementdelay="0"
						data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:10;scaleY:10;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-endspeed="1500"
						data-endeasing="Power3.easeInOut"
						data-captionhidden="on"
						style="font-size:60px;line-height:110px;">Best Escape Room Designs 
					</div>

					<div class="tp-caption mediumlarge_light_white script-font customin customout tp-resizeme rs-parallaxlevel-4"
						data-x="center"
						data-y="280"

						data-splitin="words"
						data-elementdelay="0.05"
						data-start="1650"
						data-speed="900"
						data-easing="Back.easeOut"
						data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-endelementdelay="0.1"
						data-customout="x:-230;y:-20;z:0;rotationX:0;rotationY:0;rotationZ:90;scaleX:0.2;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%"
						data-endspeed="500"
						data-endeasing="Back.easeIn"
						data-captionhidden="on"
						style="z-index: 4; font-size:50px;"> Unique Puzzles And Story Line
					</div>

					<div class="tp-caption sfb rs-parallaxlevel-5"
						data-x="center"
						data-y="380"
						data-speed="800"
						data-start="2600"
						data-easing="Power4.easeOut"
						data-endspeed="300"
						data-endeasing="Power1.easeIn"
						data-captionhidden="off"
						style="z-index: 6;font-size:60px;color:#111">
                         Guidance Throughout The Whole Process
					</div>

				</li>


				<!-- SLIDE  -->
				<li data-transition="slidehorizontal" data-slotamount="1" data-masterspeed="1000" data-fstransition="fade" data-fsmasterspeed="1000" data-fsslotamount="7" data-saveperformance="off"  data-title="Live Escape Games">
					<!-- MAIN IMAGE -->
					<img src="<?php echo base_url();?>img/Slide1.jpg"  alt="slidebg2" data-bgfit="cover"  data-bgrepeat="no-repeat">

					<div class="tp-caption large_bold_white customin customout rs-parallaxlevel-3"
						data-x="center"
						data-y="150"

						data-splitin="chars"
						data-elementdelay="0.1"
						data-start="1250"
						data-speed="900"
						data-easing="Power3.easeOut"
						data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:6;scaleY:6;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"

						data-splitout="lines"
						data-endelementdelay="0"
						data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:10;scaleY:10;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-endspeed="1500"
						data-endeasing="Power3.easeInOut"
						data-captionhidden="on"
						style="font-size:85px;line-height:110px;">Creating Live Escape Games
					</div>
                    
				</li>
                <!-- SLIDE  -->

				<!-- SLIDE  -->
				<li data-transition="slidehorizontal" data-slotamount="1" data-masterspeed="1000" data-thumb="img/Slide3.jpg"  data-fstransition="fade" data-fsmasterspeed="1000" data-fsslotamount="7" data-saveperformance="off"  data-title="Design Custom Game">
					<!-- MAIN IMAGE -->
					<img src="<?php echo base_url();?>img/Slide3.jpg"  alt="video_woman_cover3"  data-bgfit="cover" data-bgrepeat="no-repeat">
					<!-- LAYERS -->

					<div class="tp-caption large_bold_black customin customout tp-resizeme rs-parallaxlevel-3"
						data-x="center"
						data-y="450"

						data-splitin="words"
						data-elementdelay="0.1"
						data-start="1250"
						data-speed="1500"
						data-easing="Power3.easeOut"
						data-customin="x:0;y:-20;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"

						data-splitout="lines"
						data-endelementdelay="0"
						data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:10;scaleY:10;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-endspeed="1500"
						data-endeasing="Power3.easeInOut"
						data-captionhidden="on"
						style="font-size:50px;line-height:110px; z-index:4">We Also Design A Custom Games Upon Your Wish
					</div>

				</li>
				<!-- SLIDE  -->
                
				<div class="tp-bannertimer tp-bottom"></div>
                
			</ul>
		</div>
	</div>


			<!-- END REVOLUTION SLIDER -->
    
		</div>
		<!-- /Slider Container -->

		<!-- Main Container -->
		<div class="main-wrapper">

        	<!-- Container -->
            <div class="container">
            	<div class="white-space space-big"></div>
            
            	<div class="row">
                	<div class="col-md-8 col-md-offset-2">
						<h3 class="fancy-title text-center animation fadeInUp"><span>Featured Games</span></h3>
                        <div class="white-space space-small"></div>
                    </div>
                </div>

            	<div class="row">
                	<div class="col-md-12">
						<!-- Carousel -->
						<div class="carousel-box" >
							<div class="carousel carousel-nav-out carousel-simple" data-carousel-autoplay="false" data-carousel-items="4" data-carousel-nav="false" data-carousel-pagination="false"  data-carousel-speed="1000">
								
								<?php foreach($room as $row) {?>
								<div class="carousel-item">
                                	<!-- Shop Product -->
									<a href="<?php echo base_url('games/info/'.$row->id);?>">
                                	<div class="shop-product animation fadeInUp">
										<!-- Overlay Img -->
										<div class="overlay-wrapper">
											<?php if($row->main_picture != NULL){?>
												<img src="<?php echo base_url('upload/'.$row->main_picture);?>" alt="Product 1">
											<?php }else{ ?>
												<img src="<?php echo base_url('img/no_img.jpg');?>" alt="Product 1">
											<?php } ?>
											<div class="rating"  style="direction:ltr">
												<span><?php echo $row->name;?></span>
											</div>
										</div>
                                        <!-- Overlay Img -->
                                        <div class="shop-product-info">
											<p class="product-price">$ <?php echo $row->price;?></p>
										</div>
                                	</div>
									</a>
                                    <!-- /Shop Product -->
                                </div>
								<?php } ?>
							</div>
                        </div>
                    </div>
                </div>
                   <hr/>
            </div>
			<!-- Container -->
            <div class="container">
                
            	<div class="row">
  					<div class="col-md-12 columns">
                    	<h2 class="fancy-title text-center"><span>Policies</span></h2>
                    </div>
				</div>

                <div class="row">
  					<div class="col-lg-12 col-md-12 col-sm-12 columns">
                    	<div class="iconbox vertical animation fadeInLeft">								
                            <div class="iconbox-content">  
                        		<p>Quisque sagittis lacus eu lorem sodales, id adipiscing. Aenean adipiscing, sem sit amet mollis aliquet.</p>
                        		<p>Quisque sagittis lacus eu lorem sodales, id adipiscing. Aenean adipiscing, sem sit amet mollis aliquet.</p>
                        		<p>Quisque sagittis lacus eu lorem sodales, id adipiscing. Aenean adipiscing, sem sit amet mollis aliquet.</p>
                            </div>
                    	</div>
                    </div>
				</div>
            	<div class="white-space space-medium"></div>
				<hr>				
            </div>
			<!-- /Container -->
			<!-- Container -->
			<div class="container">
            	<div class="row">
                	<div class="col-md-8 col-md-offset-2">

							<h3 class="fancy-title text-center animation fadeInUp"><span>Our Partner</span></h3>
                                          
                    	<div class="white-space space-small"></div>
                    </div>
                </div>
				<div class="row">
                	<div class="col-md-12">
                             <!-- Carousel -->
							 <center>
							<div class="carousel-box" >
								<div class="carousel carousel-simple" data-carousel-autoplay="5000" data-carousel-items="5" data-carousel-nav="false" data-carousel-pagination="false"  data-carousel-speed="1000">
                                    <div class="carousel-item">
                                        <div class="overlay-wrapper">
                                   			<img src="<?php echo base_url();?>img/demo/content/brand1-shop.png" class="img-transparency" alt="Brand 1">
                                        </div>
                                    </div>
								</div>
                            </div>
                             </center>           <!-- /Carousel -->
                    </div>
                </div>
				<hr>
            </div>
            <!-- /Container -->  
		</div>
			